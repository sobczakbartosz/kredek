﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BartoszSobczakLab1
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Button:
        /// if login and password correct: change textBoxes background, hide login button and open loggedIn Window
        /// else: show message: incorrect login or password
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLogin_Click(object sender, EventArgs e)
        {
            if (textBoxLogin.Text == "test" && textBoxPassword.Text == "test")
            {
                textBoxLogin.BackColor = Color.Orange;
                textBoxPassword.BackColor = Color.Orange;
                buttonLogin.Visible = false;
                FormLogged formLogged = new FormLogged(textBoxLogin.Text);
                formLogged.Show();
            }
            else
                MessageBox.Show("Incorrect login or password");
        }

        /// <summary>
        /// Loop examle
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonLoop_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                textBoxLogin.Text += i;
            }
        }
    }
}
