﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BartoszSobczakLab1
{
    public partial class FormLogged : Form
    {
        /// <summary>
        /// Initialize components and add user name to the labelLogin
        /// </summary>
        /// <param name="loggedLogin"></param>
        public FormLogged(string loggedUser)
        {
            
            InitializeComponent();
            labelLogin.Text += loggedUser;
        }
    }
}
